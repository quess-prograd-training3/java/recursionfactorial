package com.example;

public class FactorialNumber {
    int fact=1;

    public int findFact(int number){
        if (number>0){
            return number*(findFact(number-1));
        }else {
            return 1;
        }
    }
}

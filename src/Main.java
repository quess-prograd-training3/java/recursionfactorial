import com.example.FactorialNumber;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        FactorialNumber factorialNumber=new FactorialNumber();
        System.out.println(factorialNumber.findFact(scanner.nextInt()));
    }
}